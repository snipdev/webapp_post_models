import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='snippet-models',
    version='0.1.57',
    packages=find_packages(),
    install_requires=[
        'Django==1.11.12',
        'django-model-utils==3.0.0',
        'django-imagekit==4.0.2',
        'Pillow'
    ],
    include_package_data=True,
    description='Models used by Readers & Creators web apps',
    long_description=README,
    url='snip',
    author='snip',
    author_email='info@snip.today'
)
