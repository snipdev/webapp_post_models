from django.apps import AppConfig


class SnippetModelsConfig(AppConfig):
    name = 'snippet_models'
