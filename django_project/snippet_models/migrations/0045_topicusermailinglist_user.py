# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-08-16 02:06
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('snippet_models', '0044_auto_20180816_0051'),
    ]

    operations = [
        migrations.AddField(
            model_name='topicusermailinglist',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
