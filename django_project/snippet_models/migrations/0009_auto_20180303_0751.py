# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-03-03 07:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_models', '0008_auto_20180302_0623'),
    ]

    operations = [
        migrations.AddField(
            model_name='providerimageholder',
            name='img_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='providerimageholder',
            name='license',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='providerimageholder',
            name='license_url',
            field=models.URLField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='providerimageholder',
            name='highres_url',
            field=models.URLField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='providerimageholder',
            name='thumb',
            field=models.URLField(max_length=1000),
        ),
    ]
