# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-08-13 21:37
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('snippet_models', '0041_auto_20180813_2057'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='tagsubscriptions',
            unique_together=set([('user', 'tag')]),
        ),
    ]
