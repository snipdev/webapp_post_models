# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-04-17 23:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_models', '0021_post_breaking_news'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['priority']},
        ),
        migrations.AddField(
            model_name='category',
            name='priority',
            field=models.IntegerField(default=0),
        ),
    ]
