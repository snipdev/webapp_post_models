import os.path

from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from imagekit.models import ProcessedImageField
from model_utils import Choices
from pilkit.processors import ResizeToFit


class Category(models.Model):
    name = models.CharField(max_length=30, db_index=True)
    priority = models.IntegerField(default=0)

    def __str__(self):
        return '{} - {}'.format(self.name, self.priority)

    class Meta:
        ordering = ['priority']


class PostTag(models.Model):
    name = models.CharField(max_length=50, primary_key=True)
    tag_categories = models.ManyToManyField(Category)
    subscribers = models.ManyToManyField(User, through="TagSubscriptions")
    created_at = models.DateTimeField(auto_now_add=True)
    is_category_tag = models.BooleanField(default=False)
    writer_taggable = models.BooleanField(default=True)
    promoted = models.BooleanField(default=False)
    enabled = models.BooleanField(default=True)


class TagSubscriptions(models.Model):
    DAILY = 'Daily'
    WEEKLY = 'Weekly'
    MONTHLY = 'Monthly'

    FREQUENCY_OPTIONS = Choices(DAILY, WEEKLY, MONTHLY)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tag = models.ForeignKey(PostTag, on_delete=models.CASCADE)
    frequency = models.CharField(max_length=10, choices=FREQUENCY_OPTIONS)
    enabled = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('user', 'tag')


class TopicUserMailingList(models.Model):
    DAILY = 'Daily'
    WEEKLY = 'Weekly'
    MONTHLY = 'Monthly'

    FREQUENCY_OPTIONS = Choices(DAILY, WEEKLY, MONTHLY)

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    email = models.EmailField()
    topic = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)
    frequency = models.CharField(max_length=10, choices=FREQUENCY_OPTIONS, default=DAILY)
    enabled = models.BooleanField(default=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        unique_together = ('email', 'topic')


class AbstractLock(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def time_left(self):
        time_since_created = (timezone.now() - self.created_at).total_seconds()
        return self.duration - time_since_created

    class Meta:
        abstract = True


class PostWriterOfferLock(AbstractLock):
    LOCK_DURATION = 10 * 60  # sec
    duration = models.IntegerField(default=LOCK_DURATION)


class PostEditorOfferLock(AbstractLock):
    LOCK_DURATION = 6 * 60  # sec
    duration = models.IntegerField(default=LOCK_DURATION)


class ProviderImageHolder(models.Model):

    SHUTTERSTOCK = 'SHUTTERSTOCK'
    GETTY = 'GETTY'
    PIXABAY = 'Pixabay'
    PEXELS = 'Pexels'
    FLICKR = 'Flickr'
    WIKIPEDIA = 'Wikipedia'

    SHUTTERSTOCK_ATTRIBUTION = 'Shutterstock'
    GETTY_ATTRIBUTION = 'Getty Images'
    PIXABAY_ATTRIBUTION = 'Pixabay'
    PEXELS_ATTRIBUTION = 'Pexels'
    FLICKR_ATTRIBUTION = 'Flickr'
    WIKIPEDIA_ATTRIBUTION = 'Wikipedia'

    SOURCES_OPTIONS = Choices(GETTY, SHUTTERSTOCK, PIXABAY, PEXELS, FLICKR, WIKIPEDIA)

    source = models.CharField(max_length=15, choices=SOURCES_OPTIONS)
    img_id = models.CharField(max_length=40)
    by = models.CharField(max_length=100, null=True, blank=True)
    thumb = models.URLField(max_length=1000)
    highres_url = models.URLField(max_length=1000, null=True, blank=True)

    img_name = models.CharField(max_length=150, null=True, blank=True)
    license = models.CharField(max_length=200, null=True, blank=True)
    license_url = models.URLField(max_length=1000, null=True, blank=True)
    downloaded = models.BooleanField(default=False)

    def __str__(self):
        return '{} - {} - {}'.format(self.source, self.img_id, self.by)


def filename_shortner(filename):
    MAX_LENGTH = 60
    if len(filename) > MAX_LENGTH:
        # use only the first 55 chars of basename
        basename, ext = os.path.splitext(filename)
        truncated_image_filename = basename[:(MAX_LENGTH - 5)] + ext
        filename = truncated_image_filename
    return filename


def truncate_image_filename(instance, filename):
    now = timezone.now()
    year = now.strftime("%Y")
    month = now.strftime("%m")
    path = 'images/{}/{}/'.format(year, month)

    filename = filename_shortner(filename)

    return path + filename


class PostImage(models.Model):
    FINAL_THUMB_WIDTH = 700
    FINAL_THUMB_QUALITY = 70

    image = models.ImageField(upload_to=truncate_image_filename, null=True, blank=True)
    caption = models.TextField(null=True, blank=True)
    image_holder = models.ForeignKey(ProviderImageHolder, null=True, blank=True)

    top_left_x = models.FloatField(null=True)
    top_left_y = models.FloatField(null=True)
    width = models.FloatField(null=True)
    height = models.FloatField(null=True)

    thumb_width = models.FloatField(null=True)
    thumb_height = models.FloatField(null=True)

    cropped_image = models.ImageField(upload_to='images/edited/%Y/%m/', null=True, blank=True)
    final_image_thumb = ProcessedImageField(upload_to='images/thumb/%Y/%m/',
                                            processors=[ResizeToFit(FINAL_THUMB_WIDTH, FINAL_THUMB_WIDTH)],
                                            format='JPEG',
                                            options={'quality': FINAL_THUMB_QUALITY},
                                            null=True, blank=True)
    deleted = models.BooleanField(default=False)

    @property
    def get_image(self):
        if self.cropped_image:
            return self.cropped_image
        else:
            return self.image

    def __str__(self):
        im_text = ''
        if self.image:
            im_text = self.image.name
        elif self.image_holder:
            im_text = self.image_holder.__str__()
        return '{} - {}'.format(self.caption, im_text)


class AbstractPost(models.Model):
    REPORT = 'REPORT'
    EXPLAINED = 'EXPLAINED'

    title = models.CharField(max_length=150, null=True, blank=True, default='')
    subtitle = models.TextField(null=False, blank=True, default='')
    body = models.TextField(blank=True, null=True, default='')
    writer = models.ForeignKey(User, related_name='%(class)s_writer', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    post_type = models.CharField(max_length=20, default=REPORT, db_index=True)
    image1 = models.ForeignKey(PostImage, null=True, blank=True)
    audio = models.FileField('Audio', upload_to='audio/', null=True, blank=True)
    categories = models.ManyToManyField(Category)
    tags = models.ManyToManyField(PostTag)
    class Meta:
        abstract = True

    def __str__(self):
        return '%s' % self.title

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(AbstractPost, self).save(force_insert, force_update, *args, **kwargs)

        if not self.image1:
            self.image1 = PostImage.objects.create()

    @property
    def title_str(self):
        title = self.title
        if not title:
            title = ''

        if self.post_type == self.EXPLAINED:
            title = 'Explained: ' + title

        return title

    @property
    def simple_body(self):
        b = self.body
        if b is None:
            b = ''
        return '{} {}'.format(self.subtitle, b) if self.post_type == Post.REPORT else b


class Post(AbstractPost):

    IN_WRITING = 'IN_WR'
    FINAL = 'FI'
    DELETED = 'DEL'

    # Not Used
    AWAITING_EDITOR = 'AW_ED'
    IN_EDITING = 'IN_ED'
    EDITOR_RESPOND = 'ED_RE'
    WRITER_RESPOND = 'WR_RE'
    APPROVED = 'AP'
    EDITOR_SUGGEST_FINAL = 'ED_SU'
    FREE = 'FR'

    STATUS_CHOICES = (
        (IN_WRITING, 'In writing process'),
        (FINAL, 'Final'),
        (DELETED, 'Deleted'),
        # Not Used
        (IN_EDITING, 'In editing process'),
        (EDITOR_RESPOND, 'Editor responded'),
        (WRITER_RESPOND, 'Writer responded'),
        (APPROVED, 'Editor approved'),
        (EDITOR_SUGGEST_FINAL, 'Editor final version'),
        (FREE, 'Free'),
        (AWAITING_EDITOR, 'Submitted to editor'),
    )

    first_published_at = models.DateTimeField(null=True, blank=True, db_index=True)
    slug = models.SlugField(max_length=150, null=True, blank=True)
    breaking_news = models.BooleanField(default=False)
    similar_posts = models.ManyToManyField("self", blank=True)
    status = models.CharField(max_length=5, choices=STATUS_CHOICES, default=IN_WRITING, db_index=True)

    # Unused Fields
    writer_started_at = models.DateTimeField(null=True, blank=True)
    editor = models.ForeignKey(User, related_name='%(class)s_editor', null=True, blank=True)
    editor_started_at = models.DateTimeField(null=True, blank=True)
    user_gen_topic = models.BooleanField(default=False)
    crawler_id = models.CharField(max_length=20, null=True, blank=True)

    @staticmethod
    def get_status_choices():
        return dict(Post.STATUS_CHOICES)

    @property
    def is_live(self):
        return self.status == self.FINAL

    @property
    def get_editor_action(self):
        options = {self.AWAITING_EDITOR: "Select Snippet",
                   self.WRITER_RESPOND: "View Writer Response",
                   self.EDITOR_RESPOND: "Awaiting Writer"}
        try:
            return options[self.status]
        except KeyError:
            return "Continue editing"

    @property
    def get_writer_action(self):
        options = {self.FREE: "Select Topic",
                   self.EDITOR_RESPOND: "View Editor Comments",
                   self.IN_EDITING: "In Editing",
                   self.AWAITING_EDITOR: "Awaiting Editor",
                   self.WRITER_RESPOND: "Awaiting Editor Response",
                   self.APPROVED: "In Final Edit",
                   }
        try:
            return options[self.status]
        except KeyError:
            return "Continue snipping"

    @property
    def is_waiting_for_writer(self):
        return self.status in self.STATUS_WAITING_FOR_WRITER

    @property
    def is_waiting_for_editor(self):
        return self.status in self.STATUS_WAITING_FOR_EDITOR

    @property
    def is_offered_to_writer(self):
        return self.status == self.FREE

    @property
    def is_published(self):
        return self.status == self.FINAL

    @property
    def is_offered_to_editor(self):
        return self.status == self.AWAITING_EDITOR

    def __str__(self):
        return '{} - {}'.format(self.id, self.title)


class Draft(AbstractPost):
    PUBLISHED = "PB"
    EDIT = "EDIT"

    STATUS_CHOICES = (
        (PUBLISHED, "Published"),
        (EDIT, "In Edit")
    )

    post = models.ForeignKey(Post, null=True, on_delete=models.SET_NULL)
    status = models.CharField(max_length=5, choices=STATUS_CHOICES, default=EDIT, db_index=True)
    name = models.TextField(null=True)


class CategoryRawData(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=30)
    full_name = models.CharField(max_length=150)
    relevance = models.FloatField(null=True)

    def __str__(self):
        return '{} - {}'.format(self.post, self.full_name)


class AbstractLink(models.Model):
    post = models.ForeignKey(Post, related_name='%(class)s_links', null=True)
    draft = models.ForeignKey(Draft, related_name='%(class)s_links', null=True)
    url = models.CharField(max_length=1500)

    class Meta:
        abstract = True


class InitLink(AbstractLink):
    TITLE_MAX_LEN = 200
    title = models.CharField(max_length=TITLE_MAX_LEN, null=True, blank=True)
    show_to_writer = models.BooleanField(default=True)


class Reference(AbstractLink):
    caption = models.CharField(max_length=80)
    image_url = models.URLField(max_length=1000, null=True, blank=True)
    link_title = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return 'url: {}, caption: {}'.format(self.url, self.caption)


class SocialMediaPosts(models.Model):

    post_on_twtr = models.BooleanField('Post on Twitter', default=False)
    twtr_post_id = models.CharField('Twitter ID', max_length=50, null=True, blank=True)
    twtr_publish_date = models.DateTimeField(null=True, blank=True)

    post_on_fb = models.BooleanField('Post on Facebook', default=False)
    fb_post_id = models.CharField('Facebook ID', max_length=50, null=True, blank=True)
    fb_publish_date = models.DateTimeField(null=True, blank=True)


class LinkInline(admin.StackedInline):
    model = InitLink
    fk_name = "post"
    extra = 2
    list_display = [field.name for field in InitLink._meta.fields if field.name != "id"]


class ReferenceInline(admin.StackedInline):
    model = Reference
    fk_name = "post"
    extra = 0
    list_display = [field.name for field in InitLink._meta.fields if field.name != "id"]


class PostPayment(models.Model):
    WRITER = 'writer'
    EDITOR = 'editor'
    ROLES = Choices(WRITER, EDITOR)

    ETH = 'eth'
    SNIP = 'snip'
    USD = 'usd'
    PAYMENT_METHODS = Choices(ETH, SNIP, USD)

    role = models.CharField(max_length=10, choices=ROLES)
    user = models.ForeignKey(User, related_name='post_payments')
    post = models.ForeignKey(Post, null=True, related_name='payments')
    amount = models.FloatField(null=True, blank=True, db_index=True)
    amountSnip = models.FloatField(null=True)
    amountEth = models.FloatField(null=True)
    date = models.DateTimeField(auto_now_add=True, null=True)
    paid = models.BooleanField(default=False)
    pending = models.BooleanField(default=False)
    payment_method = models.CharField(max_length=4, null=True)
    transaction_hash = models.CharField(null=True, max_length=66)

    def __str__(self):
        return 'user: {}, post: {}, amount: {}, amountSnip: {}, method: {}'.format(self.user, self.post, self.amount,
                                                                                   self.amountSnip, self.payment_method)


class PostRankingData(models.Model):
    post = models.OneToOneField(Post, on_delete=models.CASCADE)
    internal_score = models.FloatField(default=0)
    users_score = models.FloatField(default=0)
    internal_score_norm = models.FloatField(default=0)

    def __str__(self):
        return 'post: {}, internal_score: {}, users_score: {}'.format(self.post, self.internal_score, self.users_score)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Post._meta.fields if field.name != "id"]
    search_fields = ('title', 'writer__first_name')
    inlines = [LinkInline, ReferenceInline]


@admin.register(ProviderImageHolder)
class ProviderImageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProviderImageHolder._meta.fields if field.name != "id"]


@admin.register(PostImage)
class PostImageAdmin(admin.ModelAdmin):
    list_display = ['caption', 'image', 'image_holder', 'cropped_image']


@admin.register(PostWriterOfferLock)
class PostWriterOfferLockAdmin(admin.ModelAdmin):
    # list_display = ['user', 'posts', 'created_at']

    def posts(self, obj):
        return list(obj.post_set.all().values_list('id', flat=True))


@admin.register(PostPayment)
class PostPaymentAdmin(admin.ModelAdmin):
    list_display = [field.name for field in PostPayment._meta.fields if field.name != "id"]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Category._meta.fields if field.name != "id"]
